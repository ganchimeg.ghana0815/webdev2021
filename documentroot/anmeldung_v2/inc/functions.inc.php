<?php
function file_upload() {
    // erzeugt eine (ziemlich sichere) eindeutige zeichenkette (um nicht bestehende daten zu überschreiben):
    $filename = uniqid();

    // Array, welche Extensions erlaubt sind:
    $allowed = [ "jpg", "png", "jpeg", "pdf", "gif" ];

    // liefert die extension als datatype string:
    $ext = pathinfo($_FILES['certificate']['name'], PATHINFO_EXTENSION);

    // prüft, ob die extension (string) in array $allowed existiert:
    if (!in_array($ext, $allowed)) {
        return false;
    }
    // dateiname:
    $orig_name = pathinfo($_FILES['certificate']['name'], PATHINFO_FILENAME);
    // zusammensetzen: bspw.: "katze34344334ID.jpg"
    // $filename = $orig_name . $filename . "." . $ext;
    $filename = "$orig_name$filename.$ext";

    // Hier wird die hochgeladene Datei (befindet sich zunächst in einem TMP Folder)
    // nach ../../uploads kopiert - hier: AUßERHALB von document_root, da wir verhindern wollen,
    // dass irgendwie direkter zugriff auf die datei möglich ist...
    // (muss man nicht machen, bei nicht-sensiblen daten)...
    $status = move_uploaded_file($_FILES['certificate']['tmp_name'], "data/$filename");
    if ($status) {
        return $filename;
    } else {
        return false;
    }
}

function check_login() {
    // flush();
    // session_start: prüft ob ein Cookie gesendet wurde, und ob es am Server dazugehörig ein Cookie gibt
    // Falls ja: dann werden die Werte der Session (gespeichert am Server) in das $_SESSION Array übertragen...
    // Falls nein: dann wird ein Header gesetzt und als Response dem Browser mitgeteilt: Set-Cookie: Session-Cookie-Hash...
    session_start();
    if (!isset($_SESSION["login"]) || $_SESSION["login"] !== "ok") {
        // Wir merken uns zusätzlich in einer Session-Variable die Seite, auf der wir jetzt gerade sind:
        // das ist nicht die Datei, wo hier die Funktion definiert ist, sondern die URL des Scripts, das augerufen wurde...
        // ... und das schreibt PHP für uns automatisch in die Superglobale $_SERVER mit Key "PHP_SELF":
        // Warum? siehe login.php
        $_SESSION["referer"] = $_SERVER["PHP_SELF"];
        // Wenn die "Session-Variable", d.h. der Key "login" nicht gesetzt ist, bzw. nicht "ok" enthält, dann Umleitung:
        header("Location: login.php");
        // !!! WICHTIG: bei Umleitungen durch einen HTTP-Header, IMMER danach exit (Skriptabbruch)...
        // nicht nur, falls die Umleitung scheitert...
        // WICHTIGER: das script wird sonst nämlich auf jeden fall bis zum ende ausgeführt und auch im response dem client geschickt...
        // vgl.: curl "http://localhost/documentroot/anmeldung_v2/list.php"
        exit;
    }
}
