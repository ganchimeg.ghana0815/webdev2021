<?php
require_once "inc/config.inc.php";
require_once "inc/functions.inc.php";
session_start();

$errormsg = "";
$user = "admin";
// Der Hash wird mit password_hash erzeugt:
$pwd = '$2y$10$miegTYSmv4AJylKGBcCtkOg2lT814cntWNjyNCE/nLhHNUUFhpUda';

if (count($_POST) > 0) {
    // password_verify prüft, ob ein Passwort mit einem Hash zusammenpasst...
    if ($_POST["username"] === $user && password_verify($_POST["password"], $pwd)) {
        // Wenn Login ok ist, dann merken wir uns den Loginstatus, damit wir nächstes mal NICHT umgeleitet (zum login) werden.
        $_SESSION["login"] = "ok";
        // Außerdem leiten wir zurück, wo wir ursprünglich waren (das haben wir uns in dieser Session-Variable gespeichert):
        header("Location: " . $_SESSION["referer"]);
    } else {
        $errormsg = "Login ist falsch!";
    }
}

include "templates/header.tmpl.php";
// TODO: ?? operator - KOMMENTIEREN UND NOCHMALS AUSFÜHREN...
?>
<h1>Login</h1>
<?= $errormsg ?>
<form method="post" action="login.php">
    <div class="mb-3">
        <label for="username" class="form-label">Username</label>
        <input type="text" name="username" class="form-control" id="username" value="<?= $_POST["username"] ?? ""; ?>">
    </div>
    <div class="mb-3">
        <label for="password" class="form-label">Password</label>
        <input type="password" name="password" class="form-control" id="password">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
<?php
include "templates/footer.tmpl.php";
?>
