<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<form action="addiere.php" method="POST">
    Addiere:
        <input type="text" name="value1" value="">
        <select name="operation">
            <option value="+">addiere</option>
            <option value="-">subtrahiere</option>
            <option value="*">multipliziere</option>
            <option value="/">dividiere</option>
        </select>
        <input type="text" name="value2" value="">
    <br>
    <input type="submit" value="Submit">
</form>
<hr>
<?php
if ( isset( $_POST["value1"] ) ) {
    if ( is_numeric($_POST["value1"]) && is_numeric($_POST["value2"]) ) {
        echo "Ihr Ergebnis:<br>";
        // TODO: überprüfen, welche operation...
/*
        if ($_POST["operation"] == "+") {
            echo $_POST["value1"] + $_POST["value2"];
        }
        elseif ($_POST["operation"] == "-") {
            echo $_POST["value1"] - $_POST["value2"];
        }
        elseif ($_POST["operation"] == "*") {
            echo $_POST["value1"] * $_POST["value2"];
        }
        elseif ($_POST["operation"] == "/") {
            echo $_POST["value1"] / $_POST["value2"];
        }
        else {
            echo "keine Operation gewählt!";
        }
*/

        switch ($_POST["operation"]) {
            case "+":
                echo $_POST["value1"] + $_POST["value2"];
                break;
            case "-":
                echo $_POST["value1"] - $_POST["value2"];
                break;
            case "*":
                echo $_POST["value1"] * $_POST["value2"];
                break;
            case "/":
                if ($_POST["value2"] != 0) {
                    echo $_POST["value1"] / $_POST["value2"];
                } else {
                    echo "Division durch 0 nicht erlaubt!";
                }
                break;
            default:
                echo "keine Operation gewählt!";
        }

    } else {
        echo "Bitte nur Zahlen eingeben!";
    }
}
?>
</body>
</html>
