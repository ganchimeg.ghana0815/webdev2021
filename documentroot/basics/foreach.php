<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        $liste = [ 1, 2, 3, 5, 7, 11, 13 ];
        $count = count($liste); // 7

        for ($i = 0; $i < $count; $i++) {
            $liste[$i] = $liste[$i] * $liste[$i];
            echo $liste[$i];
            echo "<br>";
        }
        var_dump($liste);
        echo "<hr>";

        $liste = [ 1, 2, 3, 5, 7, 11, 13 ];
        foreach ($liste as &$element) {
            // $element = &$liste[$i];
            $element = $element * $element;
            echo $element;
            echo "<br>";
        }
        var_dump($liste);
    ?>
</body>
</html>
