<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<?php
// Funktionsdeklaration (muss nicht vor dem Aufruf stehen - die wird schon vor Laufzeit deklariert)
// Syntax: Funktionsnamen: wie Variablen; PSR: kleingeschrieben, klein_geschrieben oder camelCase...
// 1. Variablen IN in einer Funktion sind immer LOKAL - sind neu, unabhängig von globalen Variablen!
// d.h. wenn globale Werte verarbeitet werden sollen, dann müssen sie über PARAMENTER / ARGUMENTE übergeben werden!
// WICHTIG: wenn ein Ergebnis zu erwarten ist, dann wird der Wert über return (Rückgabewert) an die
// aufrufende Stelle zurückgegeben - dort ist der Ausdruck der Funktion dann genau der Wert des Rückgabewertes...
function my_function($firstname, $lastname = "Doe") {
    $name = "$firstname $lastname";
    $name = strtoupper($name);
    // return beendet immer die funktion und liefert den Wert!
    return $name;
}

$vorname = "Markus";
// Hier ergibt der Ausdruck "my_function(...)" genau den Wert, was die Funktion eben liefert...
$name = my_function("markus", "huber");
echo $name;

?>
</body>
</html>
