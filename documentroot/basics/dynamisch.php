<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        // Einzeiliger Kommentar
        echo "Hallo<br>";

        /*
            Mehrzeiliger Kommentar
            - TODO: erkläre ALLES
        */
        echo "Hallo Welt<br>";
        echo "Hallo 2<br>";
    ?>
    <hr>
    <?php
        echo "Zwei";
    ?>
</body>
</html>
