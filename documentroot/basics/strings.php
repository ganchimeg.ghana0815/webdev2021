<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        // Strings mit doppelten "" oder einfachen '' - prinzipiell gleich, ABER:
        $name = "Markus";
        $var = "Hallo Welt";
        $var = 'Hallo Welt';
        // bspw.: wenn innerhalb eines Strings andere Strings für bspw. HTML ausgebe, kann man wechselseitig begrenzen...
        echo "<div class='mystyle'>";
        echo "<hr>";

        // Unterschied: "" interpretieren Variablen, Steuerzeichen, etc.
        echo "Hallo $name\n\n\n\ntest\n";
        // man kann natürlich aber immer "escapen":
        echo "Hallo \$name";
        echo "<hr>";

        // während die '' BUCHSTÄBLICH ausgeben (aber man kann natürlich kombinieren und zusammensetzen):
        echo 'Das kostet $USD 10\n\n\n' . "\n\n\n\n";

        // var_dump("3" . "5");
        // in PHP ist + IMMER numerisch (anders als in JavaScript), und der . IMMER String-Verkettung:
        echo "5" + "3 Bier" . " sind zuviel!";

        echo "<hr>";
        $var = "Peter";
        $liste = [];
        $liste["key"] = "mega";
        echo "{$var}s Fahrrad ist {$liste["key"]} super."
    ?>
</body>
</html>
