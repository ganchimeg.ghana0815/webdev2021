<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        /*
            2-Dimensionales Array, oder "Unterarrays", oder "Unterordner"
        */
        $meta = [
            // erste Ebene - indiziert... hier Index 0
            [
                // zweite Ebene ("Unterarray"), assoziativ,
                // 3 Keys: "firstname", "lastname", "city"... und dazugehörig die values
                "firstname" => "Markus",
                "lastname" => "Huber",
                "city" => "Wien"
            ],
            // erste Ebene - indiziert... hier Index 1
            [
                // zweite Ebene ("Unterarray"), assoziativ,
                // 3 Keys: "firstname", "lastname", "city"... und dazugehörig die values
                "firstname" => "Jonas",
                "lastname" => "Witt",
                "city" => "Wien"
            ]
        ];

        // alternative Schreibweise
        $meta = array(
            array(
                "firstname" => "Markus",
                "lastname" => "Huber",
                "city" => "Wien"
            ),
            array(
                "firstname" => "Jonas",
                "lastname" => "Witt",
                "city" => "Wien",
                // "test" => array(2, 3, 4, 5, 6)
            )
        );


        echo $meta[0]["lastname"];
        // echo $meta[1]["test"][2]; // liefert 4
    ?>
</body>
</html>
