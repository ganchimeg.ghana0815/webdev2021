<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        // $arr = [ 5, 8, 12, 20, "a" ];
        $arr = [];

        $arr[] = "erster wert";
        $arr[] = 5;
        $arr[] = 8;
        $arr[] = 12;

        $arr[2] = "neu";

        // var_dump($arr);
        // echo $arr[2];

        $row = [
            "firstname" => "Markus",
            "lastname" => "Huber",
            "city" => "Wien"
        ];

        $row = [];
        $row["firstname"] = "Markus";
        $row["lastname"] = "Huber";
        $row["city"] = "Wien";

        // var_dump($row);

        $row["lastname"] = "Mustermann";

        var_dump($row);

    ?>
</body>
</html>
