<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        $liste = [ 1, 2, 3, 5, 7, 11, 13 ];
        $count = count($liste); // 7

        $i = 0;
        while ($i < $count) {
            echo $liste[$i];
            echo "<br>";
            $i++;
            /*
                $i = $i + 1;
                $i += 1;
                $i++;
            */
        }

        echo "<hr>";

        for ($i = 0; $i < $count; $i++) {
            echo $liste[$i];
            echo "<br>";
        }

        echo "<hr>";

        foreach ($liste as $element) {
            echo $element;
            echo "<br>";
        }
    ?>
</body>
</html>
