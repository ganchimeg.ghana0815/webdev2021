<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
        $var_int = 5;
        var_dump($var_int);

        echo "<hr>";

        $var_int = $var_int / 2;
        var_dump($var_int);

        echo "<hr>";

        $var_float = 5.2;
        $var_string = "5 Stunden";

        $var_bool = false;
        var_dump($var_bool);

        echo "<hr>";

        var_dump($var_float === "5.2");

        if ($var_float === "5.2") {
            echo "bedingung erfüllt";
        }

        echo "<hr>";

        var_dump(true === "asdf");

        echo "<hr> arrays: <br>";

        $arr = array();
        $arr = [];          // ist exakt das selbe wie oben

        $arr = array(5, 8, 12, 20);
        $arr = [ 5, 8, 12, 20 ]; // ist exakt das selbe wie oben

        var_dump($arr);

        echo "<hr>";

        $arr = [
            "firstname" => "Markus",
            "lastname" => "Huber",
            "postal" => 1000,
            "city" => "Wien"
        ];
        var_dump($arr);
    ?>
</body>
</html>
