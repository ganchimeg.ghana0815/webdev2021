<?php
function file_upload() {
    // erzeugt eine (ziemlich sichere) eindeutige zeichenkette (um nicht bestehende daten zu überschreiben):
    $filename = uniqid();

    // Array, welche Extensions erlaubt sind:
    $allowed = [ "jpg", "png", "jpeg", "pdf", "gif" ];

    // liefert die extension als datatype string:
    $ext = pathinfo($_FILES['certificate']['name'], PATHINFO_EXTENSION);

    // prüft, ob die extension (string) in array $allowed existiert:
    if (!in_array($ext, $allowed)) {
        return false;
    }
    // dateiname:
    $orig_name = pathinfo($_FILES['certificate']['name'], PATHINFO_FILENAME);
    // zusammensetzen: bspw.: "katze34344334ID.jpg"
    // $filename = $orig_name . $filename . "." . $ext;
    $filename = "$orig_name$filename.$ext";

    // Hier wird die hochgeladene Datei (befindet sich zunächst in einem TMP Folder)
    // nach ../../uploads kopiert - hier: AUßERHALB von document_root, da wir verhindern wollen,
    // dass irgendwie direkter zugriff auf die datei möglich ist...
    // (muss man nicht machen, bei nicht-sensiblen daten)...
    $status = move_uploaded_file($_FILES['certificate']['tmp_name'], "uploads/$filename");
    if ($status) {
        return $filename;
    } else {
        return false;
    }
}
