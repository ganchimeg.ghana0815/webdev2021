<?php
/*
include
include_once
require
require_once
*/
require_once "inc/config.inc.php";
require_once "inc/functions.inc.php";
include "templates/header.tmpl.php";
?>
        <h1><?= APP_NAME ?></h1>
        <form method="post" action="save.php" enctype="multipart/form-data">
            <div class="mb-3">
              <label for="firstname" class="form-label">Vorname</label>
              <input type="text" name="firstname" class="form-control" id="firstname">
            </div>
            <div class="mb-3">
              <label for="lastname" class="form-label">Nachname</label>
              <input type="text" name="lastname" class="form-control" id="lastname">
            </div>
            <div class="mb-3">
                <label for="email" class="form-label">E-Mail</label>
                <input type="email" name="email" class="form-control" id="email">
            </div>
            <div class="mb-3">
                <label for="svnumber" class="form-label">SV-Nummer</label>
                <input type="number" name="svnumber" class="form-control" id="svnumber">
            </div>
            <div class="mb-3">
                <label for="birthdate" class="form-label">Geburtsdatum</label>
                <input type="date" name="birthdate" class="form-control" id="birthdate">
            </div>
            <div class="mb-3">
                <label for="date" class="form-label">Termin</label>
                <input type="datetime-local" name="date" class="form-control" id="date">
            </div>
            <div class="mb-3">
                <label for="certificate" class="form-label">Impfpass</label>
                <input type="file" name="certificate" class="form-control" id="certificate">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
          </form>
<?php
include "templates/footer.tmpl.php";
?>
