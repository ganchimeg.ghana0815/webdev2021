<?php
require_once "inc/config.inc.php";
require_once "inc/functions.inc.php";
include "templates/header.tmpl.php";

/*
1. Daten Validierung - TODO
2. File-Upload (security) - DONE...
3. speichern (CSV-Datei, später: DB)
4. Response
*/

$filename = file_upload();
if (is_string($filename)) {
    $fields = $_POST;
    $fields["filename"] = $filename;
    $fp = fopen("registrations.csv", "a");
    fputcsv($fp, $fields);
    echo "Vielen Dank!";
} else {
    echo "Etwas ist schief gelaufen!";
}

include "templates/footer.tmpl.php";
