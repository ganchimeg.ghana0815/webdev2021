<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <table>
        <?php
/**
 * Notiz: ich habe den Upload-Ordner wieder INNERHALB von documentroot, also direkt innerhalb dieses Verzeichnis gelegt, ist einfacher.
 * Zusätzlich in der .htaccess des Ordners folgendes hinzugefügt: "Deny from all" - das bewirkt, dass keine Dateien darin DIREKT über HTTP aufgerufen werden können (aber mit PHP wie bisher ausgelesen werden können)...
 *
 * Pflichtübung PHP Basis:
 *
 * 1. Lese zeilenweise alle Datensätze aus "registrations.csv" und gib den Inhalt in Form einer HTML Tabelle aus.
 * Achtung (Änderung): die erste Zeile in der CSV-Datei ist die Headline/table-head (müsste bei den Berechnungen also übersprungen werden)!
 * 2. Füge in der Ausgabe eine Spalte "Alter" hinzu. Das Alter soll automatisch aus der Spalte Geburtsdatum errechnet und ausgegeben werden.
 * 3. Färbe alle Zeilen der HTML Tabelle (rows), dessen Alter >= 30 ist mit einer beliebigen Hintergrundfarbe, sowie alle mit Alter < 30 mit einer anderen Farbe.
 * 4. (freiwillig, einfach): gib am Ende die Gesamtanzahl aller Anmeldungen aus, sowie die Anzahl der Anmeldungen älter als 30 bzw. jünger als 30.
 * 5. (freiwillig, gar nicht so einfach - gemein ;-)): färbe alle Felder, wo der Wert der Spalte "Impftermin" mehrfach vorkommt (also doppelt oder dreifach vergeben), mit der Farbe rot ein!
 *
 * Die letzten beiden Punkte sind wirklich freiwillig (also 100% erhält man auch mit 1.-3., wobei das 3. zumindest versucht werden soll).
 *
 * Hilfreiche Funktionen:
 * https://www.php.net/manual/de/function.fopen.php
 * https://www.php.net/manual/de/function.fgetcsv.php
 *
 * Bei den Datumsfunktionen siehe in der Dokumentation jeweils weiter unten "Prozeduraler Stil" (gibt auch eine objektorientierte Schnittstelle):
 * https://www.php.net/manual/de/datetime.construct.php
 * https://www.php.net/manual/de/datetime.diff.php
 * https://www.php.net/manual/de/datetime.format.php
 * (Code-Beispiele für das "Age from Date" mit PHP ausrechnen gibt es aber ohnehin genug im Netz!)
 *
 * Bitte bis Mittwoch 1.12. 17:00 Uhr uploaden, Auflösung am 1.12. im Kurs :-)
 * Bei Fragen, Problemen, Schwierigkeiten, ... oder falls zu schwierig, einfach mailen: markus@bananenfisch.net
 */
        ?>
        <tr>
            <td>John</td>
            <td>...</td>
            <td><a href="get_upload.php?image=katze619563be06266.jpg" target="_blank">Show certificate</a></td>
        </tr>
    </table>
</body>
</html>
